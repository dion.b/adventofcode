use std::{fs, mem};

pub fn main() {
    parse_map("short.txt");
}

type EnergyLevel = u8;
type Point = (usize, usize);

#[derive(Debug)]
pub struct Map {
    field: Vec<Vec<EnergyLevel>>,
}

impl Map {
    fn step_map(&mut self) -> usize {
        let mut flashcount: usize = 0;
        for y in 0..10 {
            for x in 0..10 {
                flashcount = flashcount + self.step_octo(y, x)
            }
        }

        flashcount
    }

    fn step_octo(&mut self, y: usize, x: usize) -> usize {
        let cell = &mut self.field[y][x];
        *cell += 1;

        if *cell != 10 {
            return 0;
        }

        // this octopus has flashed, so now we process neighbours
        let mut flashcount = 1;
        if x > 0 {
            flashcount += self.step_octo(y, x - 1)
        }

        if y > 0 {
            flashcount += self.step_octo(y - 1, x)
        }

        if x < 9 {
            flashcount += self.step_octo(y, x + 1)
        }

        if y < 9 {
            flashcount += self.step_octo(y + 1, x)
        }

        flashcount
    }
}

pub fn parse_map(path: &str) -> Map {
    let file = fs::read_to_string("src/day11/".to_owned() + path).expect("file not found");
    let input = file.as_bytes().iter();

    let mut map: Map = Map { field: Vec::new() };
    let mut line: Vec<EnergyLevel> = Vec::new();
    input.for_each(|c| {
        match c {
            10 => map.field.push(mem::replace(&mut line, Vec::new())),
            _ => line.push(*c - 48),
        }
    });
    println!("map: {:?}", map);
    map
}
