use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

fn main() {
    let path = Path::new("short.txt");
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", display, why),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut input = String::new();
    match file.read_to_string(&mut input) {
        Err(why) => panic!("couldn't read {}: {}", display, why),
        Ok(_) => print!("{} contains:\n{}", display, input),
    }

    let lines = input.split("\n");
    let mut previous = [0, 0, 0];
    let mut count = 0;

    for line in lines {
        let next: i32 = line.parse().unwrap();
        let lastSum = previous[0] + previous[1] + previous[2];
        let nextSum = previous[1] + previous[2] + next;
        if nextSum > lastSum {
            count += 1;
        }
        previous = [previous[1], previous[2], next];
    }

    print!("\n\n{}\n", count - 3);
}
