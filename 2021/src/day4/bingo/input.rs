use super::*;

const BOARD_SIZE: usize = 5;

pub fn parse_input(input: &Vec<String>)-> game::Game{
	let mut game = game::Game::new();
	game.calls = parse_calls(&input[0]);

	let mut board_count = 0;

	while (board_count*(BOARD_SIZE+1))+2 < input.len() {
		// println!("board count: {}", board_count);
		game.boards.push(parse_board(&input[(board_count*(BOARD_SIZE+1))+2..(board_count*(BOARD_SIZE+1))+2+BOARD_SIZE]));

		board_count+=1;
	}

	return game
}

fn parse_calls(line: &String) -> Vec<isize>{
	let mut calls = Vec::new();

	for call in line.split(","){
		// println!("{}", call);
		calls.push(call.parse().unwrap())
	}

	return calls
}

fn parse_board(lines: &[String]) -> game::Board{
	// println!("{:?}", lines);
	let mut board = game::Board::new();
	for line in lines {
		// println!("{:?}",line);
		let mut row = Vec::new();
		for call in line.split(" "){
			if call.is_empty(){continue;}
			// println!("{}", call);
			row.push(call.parse().unwrap())
		}
		board.push(row);
	}
	return board
}