

pub type Board = Vec<Vec<isize>>;



pub struct Game{
	pub calls: Vec<isize>,
	pub boards: Vec<Board>,
}
 
impl Game{ 
	pub fn new() -> Self{
		Game { calls: Vec::new(), boards: Vec::new() }
	}

	pub fn play(&self) -> (usize, usize){
		let mut call_count = 5;

		while call_count < self.calls.len(){
			for (i, board) in self.boards.iter().enumerate(){
				if board_has_won(board, &self.calls[0..call_count]){
					// println!("used calls: {:?}", &self.calls[0..call_count]);
					// self.score(call_count,board);
					return (call_count,i);
				}
			}
			call_count +=1;
		}
		println!("no winner found");
		unimplemented!();
	}

	pub fn score(&self, call_count: usize, winner: &Board ){
		let calls = &self.calls[0..call_count];
		let mut sum = 0;
		for row in winner {
			'guesses: for cell in row {
				if calls.contains(cell){
					continue 'guesses;
				}
				sum += cell;
			}
		}
		println!("board sum {}", sum);

		println!("score {}", sum*calls[call_count-1]);
	}

	pub fn remove(&mut self, board: usize){
		self.boards.swap_remove(board);
		// self
	}
}



pub fn board_has_won(board: &Board, calls: &[isize]) -> bool{
	for row in board {
		if row_has_won(row, &calls) {return true}
	}
	for col in 0..board.len(){
		if col_has_won(col, &board, &calls){return true}
	}
	return false;
}

fn row_has_won(row: &Vec<isize>, calls: &[isize]) -> bool {
	'guesses: for guess in row {
		if calls.contains(guess){
			continue 'guesses;
		}
		return false
	}
	return true
}

fn col_has_won(col: usize, board: &Board, calls: &[isize]) -> bool{
	'guesses: for row in board {
		if calls.contains(&row[col]){
			continue 'guesses;
		}
		return false
	}
	return true
}