use crate::day4::bingo::game::Board;
use crate::common::files;
use super::bingo;


pub fn day4(){
	let lines = files::read_file_lines("src/day4/input.txt");
	let mut game = bingo::input::parse_input(&lines);
	// println!("{:?}", game.calls);
	while game.boards.len() > 1 {
		let (_, w) = game.play();
		game.remove(w); 
	}
	let (call_count, winning_board) = game.play();
	println!("remaining boards {}", game.boards.len());
	println!("winning call: {}\nwinning board: {:?}", call_count, game.boards[winning_board]);
	game.score(call_count, &game.boards[winning_board]);
}