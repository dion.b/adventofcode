use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

pub fn read_file_lines(name: &str) -> Vec<String> {
    let path = Path::new(name);
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let file = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", display, why),
        Ok(file) => file,
    };

    let reader = BufReader::new(file);
    reader.lines().map(|line| line.unwrap()).collect()
}
