use itertools::Itertools;
use std::{collections::HashMap, collections::HashSet, fs};

// part 1
// pub fn main() {
//     let input = fs::read_to_string("input.txt").unwrap();
//     let lines = input.split('\n');

//     let mut counter = vec![0; 10];

//     for line in lines {
//         // println!("{}", line);
//         let note = line.split('|').last().unwrap().trim_start();
//         let digits = note.split(" ");
//         for digit in digits {
//             match digit.len() {
//                 2 => counter[1] += 1,
//                 3 => counter[7] += 1,
//                 4 => counter[4] += 1,
//                 7 => counter[8] += 1,
//                 _ => {}
//             }
//         }
//     }

//     let mut sum = 0;
//     for val in counter {
//         sum += val;
//     }

//     println!("{}", sum);
// }

// part2
pub fn main() {
    // print!("{:#?}\n", std::env::current_dir());
    let input = fs::read_to_string("src/day8/input.txt").expect("file not found");
    let lines = input.split('\n');

    // let counter = vec![0; 10];
    let mut sum = 0;

    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let mut decoder = Decoder::new();
        sum += decoder.decode_line(line);
        // return;
    }

    println!("{}", sum);
}

#[derive(Debug)]
struct Decoder {
    unknown: HashMap<usize, HashSet<String>>,
    segmentptn: HashMap<char, char>,
    segmentntp: HashMap<char, char>,
    seg_to_dig: HashMap<String, usize>,
    dig_to_seg: [String; 10],
}

// digits of length
// 5: 2 3 5
// 6: 9 6 0

// known
// a c d e f g
// 0 1 4 6 7 8 9

impl Decoder {
    fn new() -> Decoder {
        let mut dec = Decoder {
            unknown: HashMap::new(),
            segmentptn: HashMap::new(),
            segmentntp: HashMap::new(),
            seg_to_dig: HashMap::new(),
            dig_to_seg: Default::default(),
        };

        dec.unknown.insert(5, HashSet::new());
        dec.unknown.insert(6, HashSet::new());
        return dec;
    }

    fn save_segment(&mut self, normal: char, puzzle: char) {
        self.segmentptn.insert(puzzle, normal);
        self.segmentntp.insert(normal, puzzle);
    }

    fn save_digit(&mut self, val: usize, digit: String) {
        let sorted = string_sort(&digit);
        self.seg_to_dig.insert(sorted.clone(), val);
        self.dig_to_seg[val] = sorted.clone();

        let uk = self.unknown.get_mut(&digit.len());
        match uk {
            Some(i) => _ = i.remove(&sorted),
            None => (),
        }
    }

    fn decode_line(&mut self, line: &str) -> u32 {
        let digits = line.split(' ');
        let mut readout: Vec<String> = Vec::new();
        let mut barfound = false;
        for digit in digits {
            if digit == "|" {
                barfound = true;
                continue;
            }
            if !barfound {
                self.decode_from_len(digit.to_string());
            } else {
                readout.push(digit.to_string());
            }
        }

        self.find_a();
        self.find_9();
        self.find_e();
        self.find_6();
        self.find_f();

        let zero = self.unknown[&6].iter().next().unwrap().clone();
        self.save_digit(0, zero);

        let dee = get_remaining_segment(&self.dig_to_seg[8], &self.dig_to_seg[0]);
        self.save_segment('d', dee);

        self.find_b();
        self.make_2();
        self.make_3();
        self.make_5();

        // println!("{:?}\n", self);

        let mut order = 1000;
        let mut value = 0;
        for digit in readout {
            let v = self.decode_digit(digit);
            value += v * order;
            order = order / 10;
        }
        println!("{}", value);

        return value;
    }

    fn decode_from_len(&mut self, i_segments: String) {
        let sorted = string_sort(&i_segments);
        let val: usize;
        match i_segments.len() {
            2 => val = 1,
            3 => val = 7,
            4 => val = 4,
            7 => val = 8,
            1 => return, // delimiter found
            i => {
                self.unknown.get_mut(&i).unwrap().insert(sorted);
                return;
            }
        }

        self.save_digit(val, sorted);
    }

    fn find_a(&mut self) {
        let a = get_remaining_segment(&self.dig_to_seg[7], &self.dig_to_seg[1]);
        self.save_segment('a', a);
    }

    // also finds g
    fn find_9(&mut self) {
        let mut known = self.dig_to_seg[4].clone();
        known.push(*self.segmentntp.get(&'a').unwrap());

        let six = self.unknown.get_mut(&6).unwrap();
        let mut found: String = String::from("");

        for digit in six.iter() {
            // print!("iter {} {}\n", i, digit);
            if digit_contains(digit, &known, &"".to_string()) {
                // print!("success 9\n");
                found = digit.to_string();
                break;
            }
        }

        self.save_digit(9, found);

        let g = get_remaining_segment(&self.dig_to_seg[9], &known);
        self.save_segment('g', g);
    }

    fn find_e(&mut self) {
        let c = get_remaining_segment(&self.dig_to_seg[8], &self.dig_to_seg[9]);
        self.save_segment('e', c);
    }

    fn find_6(&mut self) {
        let mut six: String = "".to_string();
        let mut cee: char = 'z';
        let usix = self.unknown.get_mut(&6).unwrap();

        for digit in usix.iter() {
            for c in self.dig_to_seg[1].chars() {
                if digit_contains(&digit, &self.dig_to_seg[8], &c.to_string()) {
                    // print!("success 6 {} {}\n", digit, c);
                    cee = c;
                    six = digit.to_string();
                }
            }
        }
        self.save_segment('c', cee);
        self.save_digit(6, six);
    }

    fn find_f(&mut self) {
        let f = get_remaining_segment(
            &self.dig_to_seg[1],
            &self.segmentntp.get(&'c').unwrap().to_string(),
        );
        self.save_segment('f', f)
    }

    fn find_b(&mut self) {
        let mut known: String = "".to_string();
        for (_, c) in &self.segmentntp {
            known.push(*c);
        }
        let bee = get_remaining_segment(&self.dig_to_seg[8], &known);
        self.save_segment('b', bee);
    }

    fn make_2(&mut self) {
        let three = format!(
            "{}{}{}{}{}",
            self.segmentntp.get(&'a').unwrap(),
            self.segmentntp.get(&'c').unwrap(),
            self.segmentntp.get(&'d').unwrap(),
            self.segmentntp.get(&'e').unwrap(),
            self.segmentntp.get(&'g').unwrap()
        );
        self.save_digit(2, three);
    }

    fn make_3(&mut self) {
        let three = format!(
            "{}{}{}{}{}",
            self.segmentntp.get(&'a').unwrap(),
            self.segmentntp.get(&'c').unwrap(),
            self.segmentntp.get(&'d').unwrap(),
            self.segmentntp.get(&'f').unwrap(),
            self.segmentntp.get(&'g').unwrap()
        );
        self.save_digit(3, three);
    }

    fn make_5(&mut self) {
        let three = format!(
            "{}{}{}{}{}",
            self.segmentntp.get(&'a').unwrap(),
            self.segmentntp.get(&'b').unwrap(),
            self.segmentntp.get(&'d').unwrap(),
            self.segmentntp.get(&'f').unwrap(),
            self.segmentntp.get(&'g').unwrap()
        );
        self.save_digit(5, three);
    }

    fn decode_digit(&self, digit: String) -> u32 {
        // println!("decoding {}", digit);
        let sorted = string_sort(&digit);
        for (i, d) in self.dig_to_seg.iter().enumerate() {
            if *d == sorted {
                return i as u32;
            }
        }
        return 0;
    }
}

fn digit_contains(digit: &String, includes: &String, without: &String) -> bool {
    for c in without.chars() {
        if digit.contains(c) {
            return false;
        }
    }
    for c in includes.chars() {
        if without.contains(c) {
            continue;
        }
        if !digit.contains(c) {
            return false;
        }
    }
    return true;
}

fn get_remaining_segment(digit: &String, known: &String) -> char {
    for c in digit.chars() {
        if known.contains(c) {
            continue;
        }
        return c;
    }
    return 'a';
}

fn string_sort(input: &String) -> String {
    input.chars().sorted().collect::<String>()
}
