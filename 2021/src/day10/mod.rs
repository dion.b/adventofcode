use lazy_static::lazy_static;
use std::collections::{HashMap, HashSet};

use std::{
    fs::File,
    io::{BufRead, BufReader},
};

lazy_static! {
    static ref OPENERS: HashSet<char> = vec!['(', '[', '<', '{'].into_iter().collect();
    static ref P1SCORES: HashMap<char, u32> =
        HashMap::from([(')', 3), (']', 57), ('}', 1197), ('>', 25137),]);
    static ref P2SCORES: HashMap<char, u64> =
        HashMap::from([(')', 1), (']', 2), ('}', 3), ('>', 4),]);
}

pub fn main() {
    let mut parser = new_parser("src/day10/input.txt");
    let mut score = vec![];
    loop {
        match parser.parse_line() {
            Result::Incomplete(c) => score.push(complete(c)),
            Result::Corrupt(c) => {
                // println!("char '{}'", c);
                // score = score + P2SCORES[&c];
            }
            Result::EOF => break,
        }
    }
    score.sort_unstable();
    // println!("{}", score[score.len() / 2]);
    println!("{}", score[score.len() / 2]);
}

pub struct Parser {
    input: BufReader<File>,
}

pub fn new_parser(path: &str) -> Parser {
    let file = File::open(path).expect("file not found");
    let reader = BufReader::new(file);
    Parser { input: reader }
}

enum Result {
    Corrupt(char),
    Incomplete(Vec<char>),
    EOF,
}

impl Parser {
    fn parse_line(&mut self) -> Result {
        let mut line = String::new();
        let num_bytes = self.input.read_line(&mut line).expect("won't fail");
        if num_bytes == 0 {
            return Result::EOF;
        }

        let mut stack: Vec<char> = Vec::new();

        for c in line.trim().chars() {
            if OPENERS.contains(&c) {
                stack.push(c);
                continue;
            }

            if close(*stack.last().unwrap()) == c {
                stack.pop();
                continue;
            }

            return Result::Corrupt(c);
        }

        return Result::Incomplete(stack);
    }
}

fn close(o: char) -> char {
    match o {
        '<' => '>',
        '(' => ')',
        '[' => ']',
        '{' => '}',
        _ => 'x',
    }
}

fn complete(start: Vec<char>) -> u64 {
    println!("{:?}", start);
    start
        .iter()
        .rev()
        .fold(0, |score, c| (score * 5) + P2SCORES[&close(*c)])
}
