use std::fs;
use std::mem;
use std::println;
use std::vec;

use itertools::Itertools;

pub fn main() {
    let map = parse_map("input.txt");
    let mins = map.find_low_points();

    let mut sum: i32 = 0;
    for i in mins.iter() {
        sum += (map.field[i.1][i.0] + 1) as i32
    }
    println!("sum {}", sum);
    let mut factors: Vec<usize> = Vec::new();
    for min in mins.iter() {
        factors.push(map.crawl_basin(*min));
    }

    factors.sort();

    println!(
        "factored: {}",
        factors[factors.len() - 3..].iter().fold(1, |a, b| a * b)
    )
}

pub struct Map {
    max: Point,
    field: Vec<Vec<Weight>>,
}
type Point = (usize, usize);
type Weight = u8;

impl Map {
    pub fn get(&self, p: &Point) -> Weight {
        return self.field[p.1][p.0];
    }

    pub fn find_low_points(&self) -> Vec<(usize, usize)> {
        let mut points: Vec<(usize, usize)> = Vec::new();
        for y in 0..self.field.len() {
            for x in 0..self.field[y].len() {
                if self.is_low_point((x, y)) {
                    points.push((x, y));
                }
            }
        }
        println!("{:?}", points);
        points
    }

    fn is_low_point(&self, p: Point) -> bool {
        let curr = self.field[p.1][p.0];
        let neighbours = get_neighbours(&p, &self.max);
        neighbours
            .iter()
            .map(|n| self.get(n) > curr)
            .reduce(|acc, e| acc && e)
            .unwrap()
        // let (mut left, mut right, mut up, mut down) = (true, true, true, true);
    }

    fn crawl_basin(&self, p: Point) -> usize {
        let mut touched = vec![p];
        self.basin_step(p, &mut touched);
        touched.len()
    }

    fn basin_step(&self, orig: Point, touched: &mut Vec<Point>) {
        let neighbours = get_neighbours(&orig, &self.max)
            .iter()
            .filter(|p| !(touched.contains(p) || self.get(p) == 9))
            .map(|p| *p)
            .collect_vec();
        neighbours.iter().map(|p| touched.push(*p)).collect_vec();
        neighbours
            .iter()
            .map(|p| self.basin_step(*p, touched))
            .collect_vec();
        // for ele in neighbours {
        //     touched.push(*ele)
        // }
        // .map(|p| touched.push(*p));

        // .collect::<Vec<_>>()
        // .map(|p| self.basin_step(*p, touched));
    }
}

pub fn parse_map(path: &str) -> Map {
    let file = fs::read_to_string("src/day9/".to_owned() + path).expect("file not found");
    let input = file.as_bytes().iter();

    let mut map: Map = Map {
        field: Vec::new(),
        max: (0, 0),
    };
    let mut line: Vec<Weight> = Vec::new();
    input.for_each(|c| {
        if *c == 10 {
            let tmp = mem::replace(&mut line, Vec::new());
            map.field.push(tmp);
        } else {
            line.push(*c - 48);
        }
    });
    map.max = (map.field[0].len() - 1, map.field.len() - 1);
    // println!("{:?}", map);
    map
}

fn get_neighbours(p: &Point, max: &Point) -> Vec<Point> {
    let mut points: Vec<Point> = Vec::new();
    if p.0 > 0 {
        points.push((p.0 - 1, p.1))
    }
    if p.1 > 0 {
        points.push((p.0, p.1 - 1))
    }
    if p.0 < max.0 {
        points.push((p.0 + 1, p.1))
    }
    if p.1 < max.1 {
        points.push((p.0, p.1 + 1))
    }

    points
}

// fn is_low_point(map: &Map, x: usize, y: usize) -> bool {
//     let curr = map[y][x];
//     let xmax = map[0].len() - 1;
//     let ymax = map.len() - 1;
//     // let (mut left, mut right, mut up, mut down) = (true, true, true, true);

//     return !((x > 0 && map[y][x - 1] <= curr)
//         || (y > 0 && map[y - 1][x] <= curr)
//         || (x < xmax && map[y][x + 1] <= curr)
//         || (y < ymax && map[y + 1][x] <= curr));
// }
