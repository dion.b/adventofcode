use crate::common::files;

pub fn day2p1() {
    let lines = files::read_file_lines(&"src/day2/input.txt");

    let mut depth = 0;
    let mut dist = 0;

    for line in lines {
        let parts: Vec<&str> = line.split(" ").collect();
        let val: i32 = parts[1].parse().unwrap();

        match parts[0] {
            "forward" => dist += val,
            "up" => depth -= val,
            "down" => depth += val,
            _ => unimplemented!(),
        }
    }

    print!(
        "depth: {}, distance: {}, result: {}",
        depth,
        dist,
        depth * dist,
    )
}

pub fn day2p2() {
    let lines = files::read_file_lines(&"src/day2/input.txt");

    let mut depth = 0;
    let mut dist = 0;
    let mut aim = 0;

    for line in lines {
        let parts: Vec<&str> = line.split(" ").collect();
        let val: i32 = parts[1].parse().unwrap();

        match parts[0] {
            "forward" => {
                dist += val;
                depth += aim * val
            }
            "up" => aim -= val,
            "down" => aim += val,
            _ => unimplemented!(),
        }
    }

    print!(
        "depth: {}, distance: {}, result: {}",
        depth,
        dist,
        depth * dist,
    )
}
