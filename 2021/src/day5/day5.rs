use crate::common::files;
use regex::Regex;



#[derive(Clone)]
#[derive(Debug)] 
struct Point{
	x: usize,
	y: usize,
}

impl Default for Point {
	fn default() -> Self {
	    Point { x: 0, y: 0 }
	}
}

#[derive(Clone)]
#[derive(Debug)]
struct Line{ 
	start: Point,
	end: Point,
}

impl Default for Line {
	fn default() -> Self {
	    Line { start: Point::default(), end: Point::default()}
	}
}

static INPUT_PATTERN: &str = r"(\d*),(\d*) \-> (\d*),(\d*)";

pub fn day5(){
    let re: Regex = Regex::new(INPUT_PATTERN).unwrap();

	let input_lines = files::read_file_lines("src/day5/input.txt");
	let mut lines = Vec::new();
	for line in input_lines.iter(){
		lines.push(parse_line(&re, line))
	}

	let mut field = vec![vec![0;1000];1000];

	for line in lines.iter(){
		apply_line(&mut field, line)
	}
	println!("lines {:#?}",field);

	count_intersections(field)
}

fn parse_line(pattern: &Regex, input: &String) -> Line{
	let mut ret = Line::default();
	let caps = pattern.captures(input).unwrap();

	ret.start.x = caps.get(1).unwrap().as_str().parse::<usize>().unwrap();
	ret.start.y = caps.get(2).unwrap().as_str().parse::<usize>().unwrap();
	ret.end.x = caps.get(3).unwrap().as_str().parse::<usize>().unwrap();
	ret.end.y = caps.get(4).unwrap().as_str().parse::<usize>().unwrap();

	return ret
}

fn apply_line( field: &mut Vec<Vec<i32>>, line: &Line){
	let mut step:i32 = 1;
	if line.start.x == line.end.x {
		let x = line.start.x;
		let mut y = line.start.y as i32;
		if line.start.y > line.end.y {
			step = -1;
		}
		for _ in 0..(line.end.y as i32-line.start.y as i32).abs()+1 {
			field[x][y as usize] += 1;
			y += step;
		}
	} else if line.start.y == line.end.y{
		let y = line.start.y;
		let mut x = line.start.x as i32;
		if line.start.x > line.end.x {
			step = -1;
		}
		for _ in 0..(line.end.x as i32 - line.start.x as i32).abs()+1 {
			field[x as usize][y] += 1;
			x = x + step;
		}
	} else {
		let mut y = line.start.y as i32;
		let mut x = line.start.x as i32;
		let mut xstep: i32 = 1;
		let mut ystep: i32 = 1;
		if line.start.y > line.end.y {
			ystep = -1;
		}
		if line.start.x > line.end.x {
			xstep = -1;
		}

		for _ in 0..(line.end.x as i32 - line.start.x as i32).abs()+1 {
			field[x as usize][y as usize] += 1;
			x = x + xstep;
			y = y + ystep
		}
	}
}

fn count_intersections(field: Vec<Vec<i32>>){
	let mut count = 0;
	for row in field.iter(){
		for cell in row.iter(){
			if cell > &1 {
				count += 1;
			}
		}
	}

	println!("count {}", count);
}