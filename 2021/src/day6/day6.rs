
use crate::common::files;

pub fn day6(){
	let input_lines = files::read_file_lines("src/day6/input.txt");
	let pop = &mut vec![0;10];

	for i in input_lines[0].split(','){
		pop[i.parse::<usize>().unwrap()] += 1;
	}

	println!("{:?}", pop);
	for i in 1..257 {
		step_day(pop);
		println!("day {}: {:?}",i, pop);
	}

	print!("{:?}",sum(pop));
}

fn step_day( pop: &mut Vec<i64>) -> &Vec<i64>{
	// let mut next = vec![0;9];
	pop[7] += pop[0];
	pop[9] = pop[0];
	for i in 0..9 {
		pop[i] = pop[i+1];
	}
	pop[9] = 0;

	return pop;
}

fn sum(pop: &Vec<i64>) -> i64 {
	let mut sum = 0;
	for i in pop.iter(){
		sum += i;
	}

	return sum;
}