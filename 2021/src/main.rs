#![allow(dead_code)]
#![allow(unused_variables)]
mod common;
// mod day6;
// mod day7;
mod day10;
pub mod day11;
// mod day8;
// mod day9;

fn main() {
    day11::main();
}
