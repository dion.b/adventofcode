use std::fs;

pub fn main(){
	let input = fs::read_to_string("input.txt").unwrap();
    let mut min = u32::MAX;
    let mut max = 0;
    let mut crabs = Vec::new();
    for i in input.split(','){
        let crab = i.parse::<u32>().unwrap();
        println!("{:?}", crab);
        if crab < min {
            min = crab;
        }
        if crab > max {
            max = crab;
        }
        crabs.push(crab);
    }
        println!("{:?}:{:?}", min,max);


    let mut cost = vec![0 as i64;max as usize];
    for c in crabs.iter(){
        for i in 0..cost.len() {
            // println!("{:?}", i);
            cost[i] += moveCost((i as i64 - *c as i64).abs());
        }
    }

    let mut min = i64::MAX;
    let mut pos = 0;
    for i in 0..cost.len(){
        let c = cost[i];
        if c < min {
            min = c;
            pos = i;
        }
    }

    println!("{:?}:{:?}:{:?}", min,max,pos);
}

fn moveCost(n: i64)-> i64{
    let mut sum = 0;
    for i in 0..n{
        sum += i;
    }
    sum += n;
    return sum;
}