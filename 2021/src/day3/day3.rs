

use crate::common::files;

pub fn day3() {
    let lines = files::read_file_lines("src/day3/input.txt");

    // println!("{:?}", count_bits(&lines));

    let oxygen_line = bit_criteria(&lines[..], true,0);

    let co2_line = bit_criteria(&lines[..], false,0);

    let oxval = isize::from_str_radix(&oxygen_line, 2).unwrap();
    let coval = isize::from_str_radix(&co2_line, 2).unwrap();

    println!("{},{},{:?}", oxygen_line, co2_line, oxval*coval);
}

fn count_bits(lines: &[String]) -> Vec<char> {
    let bitlen = lines[0].len();
    let mut count = 0;
    let mut accu: Vec<i32> = vec![0; bitlen];

    for line in lines {
        count += 1; 
        for i in 0..bitlen { 
            let bit = line
                .chars()
                .nth(i)
                .unwrap()
                .to_string()
                .parse::<i32>()
                .unwrap();
            accu[i] += bit;
        }
    }

    let thresh: f32 = count as f32 / 2 as f32;

    let mut most_common = Vec::new();

    for k in 0..bitlen {
        // let incr = usize::pow(2, (bitlen - k - 1) as u32);
        // print!("incr {} ", incr);
        if accu[k] as f32 >= thresh {
            most_common.push('1');
        } else {
            most_common.push('0');
        }
    }

    return most_common;
}

fn bit_criteria(lines: &[String], common: bool, n: usize) -> String {
    println!("{}", lines.len());
    let mut criteria = count_bits(lines);
    if !common {
        criteria = invert_bits(&criteria)
    }
    if lines.len() < 10 {
        println!("{} {:?} {:?}", n, criteria, lines);
    }

    // let n = lines[0].len() - criteria.len();
    let mut next_set: Vec<String> = Vec::new();

    for line in lines {
        if line.chars().nth(n).unwrap() == criteria[n] {
            next_set.push(line.to_owned());
        }
    }

    return match next_set.len() {
        1 => next_set[0].clone(),
        // 2 => find_one(n+1, &next_set),
        _ => bit_criteria(&next_set, common, n+1),
    };
}

fn find_one(index: usize, lines: &[String])->String{
    if lines[0].chars().nth(index).unwrap() == '1' {
        return lines[0].clone()
    }
    lines[1].clone()
}

fn invert_bits(inbits: &[char]) -> Vec<char>{
    let mut out = Vec::new();
    for bit in inbits{
        if *bit == '1'{
            out.push('0');
        }else{
            out.push('1');
        }
    }
    return out
}