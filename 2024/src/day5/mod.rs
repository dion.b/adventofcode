use std::cmp::Ordering;
use std::collections::HashMap;

struct Puzzle {
    orderings: HashMap<u32, Vec<u32>>,
    updates: Vec<Vec<u32>>,
}

pub fn run(input: Vec<&str>) -> String {
    let puzzle: Puzzle = input.into();

    println!("part 1: {}", part1(&puzzle));
    println!("part 2: {}", part2(&puzzle));
    "".to_string()
}

fn part1(input: &Puzzle) -> u32 {
    input
        .updates
        .iter()
        .map(|update| {
            let mut sorted = update.clone();
            sorted.sort_by(|a, b| input.order(*a, *b));
            if sorted.eq(update) {
                let mid = sorted[(sorted.len() / 2)];
                // println!("mid: {}", mid);
                mid
            } else {
                0
            }
        })
        .sum()
}

fn part2(input: &Puzzle) -> u32 {
    input
        .updates
        .iter()
        .map(|update| {
            let mut sorted = update.clone();
            sorted.sort_by(|a, b| input.order(*a, *b));
            if sorted.eq(update) {
                0
            } else {
                let mid = sorted[(sorted.len() / 2)];
                // println!("mid: {}", mid);
                mid
            }
        })
        .sum()
}

impl From<Vec<&str>> for Puzzle {
    fn from(input: Vec<&str>) -> Self {
        let mut orderings = HashMap::new();
        let mut iter = input.iter();
        iter.by_ref()
            .take_while(|line| **line != "")
            .for_each(|line| {
                let parts: Vec<&str> = line.split("|").collect();
                if !orderings.contains_key(&parts[0].parse().unwrap()) {
                    orderings.insert(parts[0].parse().unwrap(), Vec::new());
                }

                orderings
                    .get_mut(&parts[0].parse().unwrap())
                    .unwrap()
                    .push(parts[1].parse().unwrap());
            });

        let mut updates = Vec::new();
        iter.by_ref().for_each(|line| {
            let parts: Vec<u32> = line.split(",").map(|s| s.parse().unwrap()).collect();
            updates.push(parts);
        });

        Puzzle { orderings, updates }
    }
}

impl Puzzle {
    pub fn order(&self, a: u32, b: u32) -> Ordering {
        if let Some(ordering) = self.orderings.get(&a) {
            if ordering.contains(&b) {
                return Ordering::Less;
            }
        }

        if let Some(ordering) = self.orderings.get(&b) {
            if ordering.contains(&a) {
                return Ordering::Greater;
            }
        }

        Ordering::Equal
    }
}
