pub fn run(input: Vec<&str>) -> String {
    let tmp = input
        .iter()
        .map(|line| {
            line.split(" ")
                .map(|level| level.parse::<i32>().unwrap())
                .collect()
        })
        .collect::<Vec<Vec<i32>>>();

    println!("part1: {:?}", part1(&tmp));
    println!("part2: {:?}", part2(&tmp));

    "".to_string()
}

fn part1(input: &Vec<Vec<i32>>) -> i32 {
    input
        .iter()
        .map(|line| {
            // println!("{:?}", line);
            if report_is_safe(line) { 1 } else { 0 }
        })
        .sum()
}

fn report_is_safe(report: &Vec<i32>) -> bool {
    if report[0] == report[1] {
        return false;
    }
    let ascending = report[0] < report[1];
    for i in 0..report.len() - 1 {
        let mut diff = report[i + 1] - report[i];
        if !ascending {
            diff = 0 - diff;
        }
        if diff < 1 || diff > 3 {
            return false;
        }
    }
    true
}

fn part2(input: &Vec<Vec<i32>>) -> i32 {
    input
        .iter()
        .map(|line| {
            // println!("{:?}", line);
            if report_is_safe_dampened(line) { 1 } else { 0 }
        })
        .sum()
}

fn report_is_safe_dampened(report: &Vec<i32>) -> bool {
    if report_is_safe(report) {
        return true;
    }
    for i in 0..report.len() {
        let tmp = report.clone();
        let (left, right) = tmp.split_at(i);
        let joined = [left, &right[1..]].concat();
        if report_is_safe(&joined) {
            return true;
        };
    }
    false
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_report_safe() {
        assert!(report_is_safe(&vec![1, 2, 3, 4, 5, 6]));
        assert!(report_is_safe(&vec![6, 5, 4, 3, 2, 1]));
        assert!(report_is_safe(&vec![1, 2, 3, 4, 5, 6]));
        assert!(!report_is_safe(&vec![1, 1, 3, 4, 5, 6]));
        assert!(!report_is_safe(&vec![1, 2, 2, 4, 5, 6]));
        assert!(!report_is_safe(&vec![7, 2, 2, 4, 5, 6]));
        assert!(!report_is_safe(&vec![1, 2, 2, 4, 8, 6]));
    }
}
