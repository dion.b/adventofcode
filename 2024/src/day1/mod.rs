use std::collections::HashMap;

struct puzzle {
    left: Vec<i32>,
    right: Vec<i32>,
}

impl From<Vec<&str>> for puzzle {
    fn from(items: Vec<&str>) -> Self {
        let mut p = puzzle {
            left: vec![],
            right: vec![],
        };
        for line in items {
            let parts: Vec<&str> = line.split("   ").collect();
            p.left.push(parts[0].parse::<i32>().unwrap());
            p.right.push(parts[1].parse::<i32>().unwrap());
        }
        p
    }
}

impl puzzle {
    fn part1(&mut self) -> i32 {
        self.left.sort();
        self.right.sort();
        let mut ri = self.right.iter();
        self.left
            .iter()
            .map(|l| (ri.next().unwrap() - l).abs())
            .sum()
    }

    fn part2(&mut self) -> i32 {
        let mut counter: HashMap<i32, i32> = HashMap::new();
        self.right.iter().for_each(|l| {
            if let Some(x) = counter.get_mut(l) {
                *x += 1;
            } else {
                counter.insert(*l, 1);
            }
        });

        self.left
            .iter()
            .map(|l| counter.get_mut(l).and_then(|x| Some(*x * l)).unwrap_or(0))
            .sum()
    }
}

pub fn run(input: Vec<&str>) -> String {
    puzzle::from(input).part2().to_string()
}
