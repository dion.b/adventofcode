use itertools::Itertools;
use nalgebra::{Point2, Vector2};
use std::collections::HashMap;

pub fn run(input: Vec<&str>) -> String {
    let puzzle = Puzzle::from(input);
    println!("initial state: {:?}", puzzle);
    let antinodes = puzzle.find_antinodes();

    format!("{}:{:?}", antinodes.len(), antinodes)
    // format!("{}", antinodes.len())
}

#[derive(Debug)]
struct Puzzle {
    towers: HashMap<char, Vec<Point2<isize>>>,
    width: usize,
    height: usize,
}

impl From<Vec<&str>> for Puzzle {
    fn from(input: Vec<&str>) -> Self {
        let mut puzzle = Puzzle {
            height: input.len(),
            width: input[0].len(),
            towers: HashMap::new(),
        };
        input.iter().enumerate().for_each(|(y, row)| {
            row.chars().enumerate().for_each(|(x, c)| {
                if c != '.' {
                    puzzle
                        .towers
                        .entry(c)
                        .or_default()
                        .push(Point2::new(x as isize, y as isize));
                }
            })
        });
        puzzle
    }
}

impl Puzzle {
    fn find_antinodes(&self) -> Vec<Point2<isize>> {
        let mut antinodes = Vec::new();
        self.towers.iter().for_each(|(_, points)| {
            points.iter().enumerate().for_each(|(i, a)| {
                points[i + 1..].iter().for_each(|b| {
                    let dist = Vector2::new(b.x - a.x, b.y - a.y);
                    let mut p = b.clone();
                    antinodes.push(p);
                    while p.x >= 0
                        && p.x < self.width as isize
                        && p.y >= 0
                        && p.y < self.height as isize
                    {
                        p = p + dist;
                        antinodes.push(p);
                    }
                    let mut p = a.clone();
                    antinodes.push(p);
                    while p.x >= 0
                        && p.x < self.width as isize
                        && p.y >= 0
                        && p.y < self.height as isize
                    {
                        p = p - dist;
                        antinodes.push(p);
                    }
                })
            })
        });
        antinodes
            .iter()
            .filter(|p| {
                p.x >= 0 && p.x < self.width as isize && p.y >= 0 && p.y < self.height as isize
            })
            .unique()
            .cloned()
            .collect()
    }
}
