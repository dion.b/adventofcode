use crate::day9::EntryType::*;
use std::cmp::PartialEq;
use std::fmt::Write;
use std::fmt::{Debug, Display};
use std::process::exit;

pub fn run(input: Vec<&str>) -> String {
    format!("part1 {}\npart2 {}", part1(&input), part2(&input))
    // "".to_string()
}

fn part1(input: &Vec<&str>) -> String {
    let mut inflated: Vec<Option<usize>> = Vec::new();

    input[0]
        .chars()
        .map(|c| c.to_digit(10).unwrap())
        .enumerate()
        .for_each(|(i, s)| {
            if i % 2 == 0 {
                for _ in 0..s {
                    inflated.push(Some(i / 2))
                }
            } else {
                for _ in 0..s {
                    inflated.push(None)
                }
            }
        });

    compress(&mut inflated);

    // println!("{:?}", inflated);
    format!("{}", checksum(&inflated))
}

fn checksum(map: &Vec<Option<usize>>) -> usize {
    map.iter()
        .filter_map(|i| *i)
        .enumerate()
        .map(|(i, num)| i * num)
        .sum()
}

fn compress(map: &mut Vec<Option<usize>>) {
    //-> Vec<usize> {
    let mut miter = map.iter_mut();
    // let compressed: Vec<usize> = Vec::new();
    loop {
        let n = miter.next();
        if n.is_none() {
            break;
        }
        let n = n.unwrap();
        if n.is_some() {
            continue;
        }
        let next: usize;
        loop {
            let t = miter.next_back().unwrap();
            match t {
                None => continue,
                Some(T) => {
                    next = *T;
                    *t = None;
                    break;
                }
            };
        }
        *n = Some(next);
    }
}

#[derive(Clone, Debug)]
struct Entry {
    kind: EntryType,
    size: u32,
}
#[derive(Clone, Debug, PartialEq)]
enum EntryType {
    gap,
    file(u32),
}

// impl PartialEq for EntryType {
//     fn eq(&self, other: &Self) -> bool {
//         todo!()
//     }
// }

fn part2(input: &Vec<&str>) -> usize {
    let mut disk: Vec<Entry> = input[0]
        .chars()
        .map(|c| c.to_digit(10).unwrap())
        .enumerate()
        .map(|(i, s)| {
            if i % 2 == 0 {
                if s == 0 {
                    println!("file {} is empty", i / 2);
                    exit(1);
                }
                Entry {
                    kind: file((i / 2) as u32),
                    size: s as u32,
                }
            } else {
                Entry {
                    kind: gap,
                    size: s as u32,
                }
            }
        })
        .filter(|e| e.size != 0)
        .collect();

    println!("initial state");
    print_disk(&disk);

    let mut i = disk.len();

    loop {
        if i == 0 {
            break;
        }
        i -= 1;
        let f = disk[i].clone();
        if f.kind == gap {
            continue;
        }
        for j in 0..i {
            if let Some(g) = disk.get_mut(j)
                && g.kind == gap
                && f.size <= g.size
            {
                println!("moving {:?} to {}", f, j);

                if g.size == f.size {
                    *g = f;
                    delete_file(&mut disk, i);
                } else {
                    g.size -= f.size;
                    delete_file(&mut disk, i);
                    disk.insert(j, f);
                    i += 1;
                }

                break;
            }
        }
    }

    // println!("{:?}", disk);
    print_disk(&disk);
    checksum_entry(&disk)
}

fn delete_file(disk: &mut Vec<Entry>, i: usize) {
    disk[i].kind = gap;
    // disk[i - 1].size += s;
    // if i + 1 < disk.len() && disk[i + 1].kind == gap {
    //     disk[i + 1].size += disk[i].size;
    //     disk.remove(i);
    // }
    // if i > 0 && disk[i - 1].kind == gap {
    //     disk[i - 1].size += disk[i].size;
    //     disk.remove(i);
    // }
}

fn checksum_entry(map: &Vec<Entry>) -> usize {
    let mut i = 0;
    let mut sum: usize = 0;
    map.iter().for_each(|e| {
        for _ in 0..e.size {
            if let file(id) = e.kind {
                sum += i * id as usize;
            }
            i += 1;
        }
    });
    sum
}

impl Display for Entry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        let mut buf = String::from("");
        let c = match self.kind {
            gap => "_".to_string(),
            file(id) => id.to_string(),
        };
        for _ in 0..self.size {
            buf.push_str(&format!("{},", &c));
        }
        write!(f, "{}", buf)
    }
}

fn print_disk(map: &Vec<Entry>) {
    let mut buf = String::new();
    map.iter().for_each(|e| write!(buf, "{:?}|", e).unwrap());
    println!("{}", buf);
}
