use lazy_static::lazy_static;
use nalgebra::{Matrix2, Point2, Vector2};
use std::ops::Mul;

struct Puzzle {
    grid: Vec<Vec<char>>,
}

const DIRECTIONS: [Vector2<i32>; 8] = [
    Vector2::new(0, 1),
    Vector2::new(-1, 1),
    Vector2::new(-1, 0),
    Vector2::new(-1, -1),
    Vector2::new(0, -1),
    Vector2::new(1, -1),
    Vector2::new(1, 0),
    Vector2::new(1, 1),
];

const DIAGONALS: [Vector2<i32>; 4] = [
    // Vector2::new(0, 1),
    Vector2::new(-1, 1),
    // Vector2::new(-1, 0),
    Vector2::new(-1, -1),
    // Vector2::new(0, -1),
    Vector2::new(1, -1),
    // Vector2::new(1, 0),
    Vector2::new(1, 1),
];

pub fn run(input: Vec<&str>) -> String {
    let mut puzzle: Puzzle = Puzzle::new();

    for line in input.iter() {
        let x = line.chars().collect::<Vec<char>>();
        puzzle.grid.push(x);
    }

    println!("part 1: {}", part1(&puzzle));
    println!("part 2: {}", part2(&puzzle));
    "".to_string()
}

fn part1(input: &Puzzle) -> i32 {
    let mut count = 0;
    for (y, row) in input.grid.iter().enumerate() {
        for (x, letter) in row.iter().enumerate() {
            if *letter == 'X' {
                let start = Point2::new(x as i32, y as i32);
                for dir in &DIRECTIONS {
                    if input.check_for_word(&start, dir).is_some() {
                        count += 1;
                    }
                }
            }
        }
    }
    count
}

fn part2(input: &Puzzle) -> i32 {
    let mut count = 0;
    for (y, row) in input.grid.iter().enumerate() {
        for (x, letter) in row.iter().enumerate() {
            if *letter == 'A' {
                let start = Point2::new(x as i32, y as i32);
                if input.check_for_xmas(&start).is_some() {
                    count += 1;
                }
            }
        }
    }
    count
}

lazy_static! {
    pub static ref rotate: Matrix2<i32> = Matrix2::<i32>::from_vec(vec![0, 1, -1, 0]);
}

impl Puzzle {
    fn new() -> Puzzle {
        Puzzle { grid: Vec::new() }
    }
    fn get(&self, p: Point2<i32>) -> Option<char> {
        if p.x < 0 || p.y < 0 || p.y >= self.grid.len() as i32 || p.x >= self.grid[0].len() as i32 {
            return None;
        }
        Some(self.grid[p.y as usize][p.x as usize])
    }

    fn check_for_word(&self, start: &Point2<i32>, dir: &Vector2<i32>) -> Option<bool> {
        let mut p = start.clone();
        for n in "MAS".chars() {
            p = p + dir;
            let c = self.get(p)?;
            if c != n {
                return None;
            }
        }
        Some(true)
    }

    fn check_for_xmas(&self, start: &Point2<i32>) -> Option<()> {
        for dir in &DIAGONALS {
            let p = start + dir;
            if let Some(c) = self.get(p)
                && c == 'M'
            {
                if self.check_cross(start, dir).is_some() {
                    return Some(());
                }
            }
        }

        None
    }

    fn check_cross(&self, start: &Point2<i32>, dir: &Vector2<i32>) -> Option<()> {
        let mut search_dir = dir.clone();
        for n in "MSS".chars() {
            search_dir = rotate.mul(search_dir).into();
            let p = start + search_dir;
            let c = self.get(p)?;
            if c != n {
                return None;
            }
        }
        Some(())
    }
}
