use crate::day6::state::*;
use crate::day6::StepResult::{exited, progressed};
use derive;
use itertools::Itertools;
use lazy_static::lazy_static;
use nalgebra::{DMatrix, Dyn, Matrix2, OMatrix, Point2, Vector2};
use std::cmp::{Ordering, PartialEq};
use std::fmt::{Display, Formatter};
use std::mem;
use std::ops::{Add, Mul};

// correct answer for part 2 should be 2262

#[derive(Clone, Debug)]
struct Puzzle {
    map: OMatrix<state, Dyn, Dyn>,
    guard_pos: Point2<isize>,
    direction: Vector2<isize>,
    loopcount: u32,
    loops: Vec<Point2<isize>>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum state {
    free,
    obstructed,
    seen(Vector2<isize>),
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum StepResult {
    progressed,
    exited,
    looped,
}

lazy_static! {
    pub static ref rotate: Matrix2<isize> = Matrix2::<isize>::from_vec(vec![0, -1, 1, 0]);
}

pub fn run(input: Vec<&str>) -> String {
    let mut puzzle: Puzzle = input.into();

    println!("part 1: {}", part1(&mut puzzle));
    println!("part 2: {}", part2(&puzzle));
    "".to_string()
}

fn part1(input: &mut Puzzle) -> u32 {
    println!("initial state: \n{}", input);
    let p = input;
    p.visit();
    while p.step(true) == progressed {}
    println!("final state: \n{}", p);

    p.count()
}

fn part2(input: &Puzzle) -> usize {
    let loops: Vec<&Point2<isize>> = input.loops.iter().unique().collect();
    println!("loops: {:?}", loops);
    loops.len()
}

impl From<Vec<&str>> for Puzzle {
    fn from(input: Vec<&str>) -> Self {
        let mut map = DMatrix::zeros(input[0].len(), input.len());
        let mut guard_pos: Point2<isize> = Point2::<isize>::origin();
        let loopcount = 1;

        input.iter().enumerate().for_each(|(i, line)| {
            let mut row = map.row_mut(i);
            line.chars().enumerate().for_each(|(j, c)| {
                if c == '^' {
                    guard_pos = Point2::new(i as isize, j as isize);
                }
                row[j] = c.into()
            });
        });

        Puzzle {
            map,
            guard_pos,
            direction: Vector2::new(-1, 0),
            loopcount,
            loops: Vec::new(),
        }
    }
}

impl Puzzle {
    pub fn order(&self, a: u32, b: u32) -> Ordering {
        Ordering::Equal
    }

    fn pos(&self) -> (isize, isize) {
        (self.guard_pos.x, self.guard_pos.y)
    }

    fn get(&self, p: Point2<isize>) -> Option<&state> {
        self.map.get((p.x as usize, p.y as usize))
    }

    fn next(&self) -> Option<&state> {
        let p = self.guard_pos + self.direction;
        self.get(p)
    }

    fn turn(&mut self) {
        self.direction = rotate.mul(self.direction).into()
    }

    fn visit(&mut self) {
        self.map[(self.guard_pos.x as usize, self.guard_pos.y as usize)] = seen(self.direction);
    }

    pub fn step(&mut self, visit: bool) -> StepResult {
        match self.next() {
            None => return exited,
            Some(free) => {
                self.guard_pos += self.direction;
                self.visit();

                // self.count += 1;
                // println!("pos: {:?}, count {:?}", self.pos(), self.count);
            }
            Some(obstructed) => {
                self.turn();
                self.visit();
            }
            Some(seen(dir)) => {
                if dir.eq(&self.direction) {
                    // println!("looped");
                    // println!("{}", self);
                    // exit(1);
                    return StepResult::looped;
                }
                // if rotate.mul(self.direction).eq(dir) {
                //     let test = self.guard_pos + self.direction + self.direction;
                //     if let Some(cell) = self.get(test)
                //         && *cell == free
                //     {
                //         // println!("can place obstruction at {:?}", test);
                //         self.loops.push(test);
                //     }
                // }

                self.guard_pos += self.direction;
            }
        }
        if visit && self.can_loop() {
            self.loops.push(self.guard_pos + self.direction);
        }
        progressed
    }

    pub fn can_loop(&mut self) -> bool {
        let mut p = self.clone();
        p.turn();
        loop {
            match p.step(false) {
                progressed => {
                    continue;
                }
                exited => {
                    return false;
                }
                StepResult::looped => return true,
            }
        }
    }

    pub fn count(&self) -> u32 {
        self.map
            .iter()
            .filter(|e| mem::discriminant(*e) == mem::discriminant(&seen(Vector2::new(0, 0))))
            .count() as u32
    }
}

impl Display for Puzzle {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut output = Vec::new();
        for row in self.map.row_iter() {
            let mut row_string = String::new();
            for cell in row.iter() {
                row_string.push(char::from(cell));
            }
            output.push(row_string);
        }

        // output = output.into_iter().rev().collect();
        let output_string = output.join("\n");

        write!(f, "{}", output_string)
    }
}

impl Add<Self> for state {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        todo!()
    }
}

impl num_traits::identities::Zero for state {
    fn zero() -> Self {
        free
    }

    fn is_zero(&self) -> bool {
        matches!(self, free)
    }
}

impl From<char> for state {
    fn from(c: char) -> Self {
        match c {
            '.' => free,
            '#' => obstructed,
            _ => free,
        }
    }
}

// impl Into<char> for &state {
//     fn into(self) -> char {
//         match self {
//             free => '.',
//             obstructed => '#',
//             seen => 'x',
//         }
//     }
// }

impl From<&state> for char {
    fn from(i: &state) -> char {
        match i {
            free => '.',
            obstructed => '#',
            seen(_) => '*',
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_report_safe() {
        let mut dir = Vector2::new(0, 1);
        dir = rotate.mul(dir).into();
        assert_eq!(dir, Vector2::<isize>::new(1, 0));
        dir = rotate.mul(dir).into();
        assert_eq!(dir, Vector2::<isize>::new(0, -1));
        dir = rotate.mul(dir).into();
        assert_eq!(dir, Vector2::<isize>::new(0, 0));
    }
}
