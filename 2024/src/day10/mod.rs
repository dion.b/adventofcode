use nalgebra::{DMatrix, Dyn, OMatrix, Point2, Point4};
use std::collections::VecDeque;

pub fn run(input: Vec<&str>) -> String {
    // let map: OMatrix<u8, Dyn, Dyn> = DMatrix::zeros(input[0].len(), input.len());
    let map = parse_input(input);
    println!("initial state: {}", map);
    format!("part1 {}\npart2 {}", part1(&map).len(), part2(&map))
    // "".to_string()
}

type Map = OMatrix<u8, Dyn, Dyn>;

fn parse_input(input: Vec<&str>) -> Map {
    DMatrix::from_vec(
        input[0].len(),
        input.len(),
        input
            .iter()
            .map(|line| {
                line.chars()
                    .map(|h| h.to_digit(10).unwrap() as u8)
                    .collect::<Vec<u8>>()
            })
            .flatten()
            .collect::<Vec<u8>>(),
    )
}

#[derive(Debug, Clone)]
struct Path {
    start: Point2<usize>,
    route: Vec<Point2<usize>>,
    last: u8,
}

impl Path {
    fn new(x: usize, y: usize) -> Self {
        let start = Point2::new(x, y);
        Path {
            start,
            route: vec![start],
            last: 0,
        }
    }

    fn end(&self) -> &Point2<usize> {
        self.route.last().unwrap()
    }

    fn step(&self, x: usize, y: usize) -> Self {
        let mut tmp = self.clone();
        tmp.route.push(Point2::new(x, y));
        tmp.last += 1;
        tmp
    }
}
fn part1(input: &Map) -> Vec<Point4<usize>> {
    let mut roots = VecDeque::new();
    // input.iter().enumerate().for_each(|(i, line)| {})
    input.row_iter().enumerate().for_each(|(x, col)| {
        col.iter().enumerate().for_each(|(y, &c)| {
            if c == 0 {
                roots.push_back(Path::new(x, y));
            }
        })
    });

    println!("starts found: {}", roots.len());
    println!("dimensions: {} {}", input.nrows(), input.ncols());

    let mut count = vec![];

    while let Some(found) = step(&mut roots, input) {
        if found != Point4::origin() {
            count.push(found);
        }
    }
    count
    // count.into_iter().unique().collect()
}

fn step(routes: &mut VecDeque<Path>, map: &Map) -> Option<Point4<usize>> {
    let p = routes.pop_front()?;
    if p.start.x == 0 || p.start.y == 0 {
        println!("progress: {:?}", p);
    }
    let e = p.end();
    if p.last == 9 {
        println!("complete: {:?}", p);
        return Some(Point4::new(p.start.x, p.start.y, e.x, e.y));
    }
    if e.x > 0 && map[(e.x - 1, e.y)] == p.last + 1 {
        routes.push_back(p.step(e.x - 1, e.y));
    }
    if e.y > 0 && map[(e.x, e.y - 1)] == p.last + 1 {
        routes.push_back(p.step(e.x, e.y - 1));
    }
    if e.x < map.nrows() - 1 && map[(e.x + 1, e.y)] == p.last + 1 {
        routes.push_back(p.step(e.x + 1, e.y));
    }
    if e.y < map.ncols() - 1 && map[(e.x, e.y + 1)] == p.last + 1 {
        routes.push_back(p.step(e.x, e.y + 1));
    }
    Some(Point4::origin())
}

fn part2(input: &Map) -> String {
    "".to_string()
}
