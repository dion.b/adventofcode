#[derive(Debug)]
struct Puzzle {
    answer: u64,
    operands: Vec<u64>,
}

pub fn run(input: Vec<&str>) -> String {
    let sum: u64 = input
        .iter()
        .map(|line| parse_line(line))
        .map(|p| test_eq2(p))
        .sum();

    println!("part 1: {}", sum);
    // println!("part 2: {}", part2(&puzzle));
    "".to_string()
}

fn part1(input: &mut Puzzle) -> u32 {
    0
}

fn part2(input: &Puzzle) -> usize {
    0
}

fn parse_line(line: &str) -> Puzzle {
    let parts: Vec<u64> = line
        .split(" ")
        .map(|word| word.trim_end_matches(":").parse::<u64>().unwrap())
        .collect();
    Puzzle {
        answer: parts[0],
        operands: parts[1..].to_vec(),
    }
}

fn test_eq1(p: Puzzle) -> u64 {
    // println!("{:?}", p);
    // println!("pow {:?}", (p.operands.len() - 1));
    // println!("rounds {:?}", (2usize.pow((p.operands.len() - 1) as u32)));
    for operators in 0..(2usize.pow((p.operands.len() - 1) as u32)) {
        let test = p
            .operands
            .clone()
            .into_iter()
            .enumerate()
            .reduce(|s, (i, j)| {
                // println!(
                //     "{},{},{}",
                //     operators,
                //     (2usize.pow(i as u32 - 1)),
                //     operators & (2usize.pow(i as u32 - 1))
                // );
                (
                    0,
                    if operators & (2usize.pow(i as u32 - 1)) == 0 {
                        j + s.1
                    } else {
                        j * s.1
                    },
                )
            })
            .unwrap();
        // println!("{:?}, {:?}", p.answer, test.1);
        if test.1 == p.answer {
            return p.answer;
        }
    }
    0
}

fn test_eq2(p: Puzzle) -> u64 {
    // println!("{:?}", p);
    // println!("pow {:?}", (p.operands.len() - 1));
    // println!("rounds {:?}", (3usize.pow((p.operands.len() - 1) as u32)));
    for operators in 0..=3usize.pow((p.operands.len()) as u32) {
        let test = p
            .operands
            .clone()
            .into_iter()
            .enumerate()
            .reduce(|s, (i, j)| {
                // println!(
                //     "a: {},{},{}",
                //     operators,
                //     (3usize.pow(i as u32 - 1)),
                //     (operators as isize / (i * 3) as isize) % 3
                // );
                // println!("{},{}", operators, operators % 3);
                (
                    0,
                    match ((operators as isize / (3usize.pow(i as u32)) as isize) % 3) {
                        0 => j + s.1,
                        1 => j * s.1,
                        2 => format!("{}{}", s.1, j).parse::<u64>().unwrap(),
                        _ => unreachable!(),
                    },
                )
            })
            .unwrap();
        // println!("{:?}, {:?}", p.answer, test.1);
        if test.1 == p.answer {
            return p.answer;
        }
    }
    0
}
