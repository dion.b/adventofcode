use std::fmt::Display;

const NUMSTEPS: u16 = 75;

pub async fn run(input: Vec<&str>) -> String {
    let mut map = parse_input(&input);
    println!("step 0: {}", map);
    for i in 1..=25 {
        map.step();
        // println!("step {}: {}", i, map);
        println!("step {}", i);
    }
    println!("v1 complete");
    format!("part1 {}\npart2 {}", "part1", attempt4::run(input[0]).await)
    // "".to_string()
}

type Valuesize = u64;

#[derive(Debug, Clone)]
struct node {
    value: Valuesize,
    children: Vec<node>,
}

impl node {
    fn new(value: Valuesize) -> Self {
        node {
            value,
            children: Vec::new(),
        }
    }
    fn step(&mut self) {
        self.children.iter_mut().rev().for_each(|n| n.step());

        if self.value == 0 {
            self.value = 1;
        } else if let numchars = self.value.checked_ilog10().unwrap_or(0) + 1
            && numchars % 2 == 0
        {
            // println!("value: {}, numchars: {}", self.value, numchars);
            let ord = 10u64.pow(numchars / 2);
            self.children.push(node::new(self.value % ord));
            self.value = self.value / ord;
        } else {
            self.value = self.value * 2024;
        }
    }

    fn len(&self) -> u32 {
        1 + self.children.iter().map(|n| n.len()).sum::<u32>()
    }

    fn to_string(&self) -> String {
        format!(
            "{} {}",
            self.value.to_string(),
            self.children
                .iter()
                .rev()
                .map(|n| n.to_string())
                .fold("".to_string(), |mut a, v| {
                    a.push_str(&v);
                    a
                })
        )
    }
}

impl Display for node {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

fn parse_input(input: &Vec<&str>) -> node {
    let list = input[0]
        .split(" ")
        .map(|v| node::new(v.parse().unwrap()))
        .collect::<Vec<node>>();

    let (first, children) = list.split_at(1);
    let mut root = first[0].clone();
    root.children = children.iter().rev().cloned().collect();
    root
}

mod attempt2 {

    pub async fn run(input: &str) -> u64 {
        let mut js = JoinSet::new();
        input
            .split(" ")
            .for_each(|v| _ = js.spawn(start(v.parse().unwrap(), NUMSTEPS)));

        let mut sum = 0;
        while let Some(res) = js.join_next().await {
            sum += res.unwrap();
        }
        sum
    }

    use crate::day11::NUMSTEPS;
    use tokio::task::JoinSet;

    // pub async fn run(input: &str) -> u64 {
    //     input
    //         .split(" ")
    //         .map(|v| step(v.parse().unwrap(), NUMSTEPS))
    //         .sum()
    // }

    async fn start(start: u64, num_steps: u16) -> u64 {
        step(start, num_steps)
    }
    fn step(start: u64, num_steps: u16) -> u64 {
        if num_steps == 0 {
            return 1;
        }

        if start == 0 {
            step(1, num_steps - 1)
        } else if start < 10 {
            step(start * 2024, num_steps - 1)
        } else if let numchars = start.ilog10() + 1
            && numchars % 2 == 0
        {
            let ord = 10u64.pow(numchars / 2);
            step(start % ord, num_steps - 1) + step(start / ord, num_steps - 1)
        } else {
            step(start * 2024, num_steps - 1)
        }
    }
}

mod attempt3 {
    use crate::day11::NUMSTEPS;

    type valuesize = u64;

    #[derive(Debug, Clone)]
    struct node {
        value: valuesize,
        children: Vec<node>,
    }

    impl node {
        fn new(value: valuesize) -> Self {
            node {
                value,
                children: Vec::new(),
            }
        }
        fn step(&mut self) {
            self.children.iter_mut().rev().for_each(|n| n.step());

            if self.value == 0 {
                self.value = 1;
            } else if let numchars = self.value.checked_ilog10().unwrap_or(0) + 1
                && numchars % 2 == 0
            {
                // println!("value: {}, numchars: {}", self.value, numchars);
                let ord = 10u64.pow(numchars / 2);
                self.children.push(node::new(self.value % ord));
                self.value = self.value / ord;
            } else {
                self.value = self.value * 2024;
            }
        }

        fn len(&self) -> u64 {
            1 + self.children.iter().map(|n| n.len()).sum::<u64>()
        }
    }

    fn parse_input(input: &str) -> Vec<node> {
        input
            .split(" ")
            .map(|v| node::new(v.parse().unwrap()))
            .collect::<Vec<node>>()
    }

    pub async fn run(input: &str) -> u64 {
        let mut sum = 0;
        let map = parse_input(&input);
        for n in map {
            let mut n = n.clone();
            for _ in 1..=NUMSTEPS {
                n.step();
            }
            sum += n.len();
        }
        sum
    }
}

mod attempt4 {
    use crate::day11::NUMSTEPS;
    use std::collections::BTreeMap;

    type Cache = BTreeMap<(u64, u16), u64>;

    pub async fn run(input: &str) -> u64 {
        let mut cache: Cache = BTreeMap::new();
        input
            .split(" ")
            .map(|v| step(&mut cache, v.parse().unwrap(), NUMSTEPS))
            .sum()
    }

    async fn start(cache: &mut Cache, start: u64, num_steps: u16) -> u64 {
        step(cache, start, num_steps)
    }
    fn step(cache: &mut Cache, start: u64, num_steps: u16) -> u64 {
        if num_steps == 0 {
            return 1;
        }

        if start == 0 {
            return step(cache, 1, num_steps - 1);
        } else if start < 10 {
            return step(cache, start * 2024, num_steps - 1);
        }

        if let Some(v) = cache.get(&(start, num_steps)) {
            return *v;
        }

        let v = {
            let numchars = start.ilog10() + 1;
            if numchars % 2 == 0 {
                let ord = 10u64.pow(numchars / 2);
                step(cache, start % ord, num_steps - 1) + step(cache, start / ord, num_steps - 1)
            } else {
                step(cache, start * 2024, num_steps - 1)
            }
        };
        cache.insert((start, num_steps), v);
        v
    }
}
