#![feature(let_chains)]
#![feature(slice_take)]

mod day1;
mod day10;
mod day11;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

use std::{env, fs};

#[tokio::main]
async fn main() {
    let args = env::args().skip(1).collect::<Vec<_>>();
    let day = args.get(0).and_then(|s| Some(s.as_str())).unwrap_or("1");
    let name = format!(
        "src/day{}/{}.txt",
        day,
        if args.len() > 1 { "sample" } else { "puzzle" }
    );
    println!("name: {}", name);

    let raw = fs::read_to_string(name).expect("Something went wrong reading the file");
    let input = raw.split('\n').collect();
    let func = match day {
        "1" => day1::run(input),
        "2" => day2::run(input),
        "3" => day3::run(input),
        "4" => day4::run(input),
        "5" => day5::run(input),
        "6" => day6::run(input),
        "7" => day7::run(input),
        "8" => day8::run(input),
        "9" => day9::run(input),
        "10" => day10::run(input),
        "11" => day11::run(input).await,
        _ => unreachable!(),
    };

    println!("{}", func);
}
