use regex::Regex;

pub fn run(input: Vec<&str>) -> String {
    let input = input.join("\n");
    let sum = part1(&input);
    println!("part 1: {}", sum);
    println!("part 2: {}", part2(&input));
    "".to_string()
}

fn part1(input: &str) -> i32 {
    let re = Regex::new(r"mul\(([0-9]+),([0-9]+)\)").unwrap();
    re.captures_iter(&input)
        .map(|c| {
            let (_, [left, right]) = c.extract();
            (left, right)
        })
        .map(|(l, r)| l.parse::<i32>().unwrap() * r.parse::<i32>().unwrap())
        .sum()
}

fn part2(input: &str) -> i32 {
    let re = Regex::new(r"mul\(([0-9]+),([0-9]+)\)|do\(()()\)|don't\(()()\)").unwrap();
    let mut active = true;
    re.captures_iter(&input)
        .map(|c| {
            let (cmd, [left, right]) = c.extract();
            (cmd, left, right)
        })
        .map(|(cmd, l, r)| match cmd {
            "do()" => {
                active = true;
                0
            }
            "don't()" => {
                active = false;
                0
            }
            _ => {
                if active {
                    l.parse::<i32>().unwrap() * r.parse::<i32>().unwrap()
                } else {
                    0
                }
            }
        })
        .sum()
}
